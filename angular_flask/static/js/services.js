'use strict';

angular.module('angularFlaskServices', ['ngResource'])
	.factory('Post', function($resource) {
		return $resource('https://ld185e0v7l.execute-api.ap-southeast-1.amazonaws.com/dev/mockapi/profile', {}, {
			query: {
				method: 'GET',
				params: { postId: '' },
				isArray: true
			}
		});
	})
;



